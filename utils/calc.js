async function calc_all(input, health, dental) {
    input['message'] = 'Este servicio muestra el detalle del costo por cobertura';

    let total_charge = 0;
    let total_charge_by_health = 0;
    let total_charge_by_dental = 0;

    input['policy']['workers'].forEach((worker, index) => {
        let company_percentage = input['policy']['company_percentage'] / 100;
        //input['policy']['has_dental_care'] = true;
        /**
         * Primer valor es costo por seguro de salud, el segundo es por cobertura dental
         */
        worker['coverage'] = (worker["age"] < 65);
        switch (worker["childs"]) {
            case 0:
                health && worker['coverage'] ? worker["charge_by_health"] = 0.279 : 0;
                dental && worker['coverage'] && input['policy']['has_dental_care'] ? worker["charge_by_dental"] = 0.12 : 0;
                break;
            case 1:
                health && worker['coverage'] ? worker["charge_by_health"] = 0.4396 : 0;
                dental && worker['coverage'] && input['policy']['has_dental_care'] ? worker["charge_by_dental"] = 0.1950 : 0;
                break;
            default:
                health && worker['coverage'] ? worker["charge_by_health"] = 0.5599 : 0;
                dental && worker['coverage'] && input['policy']['has_dental_care'] ? worker["charge_by_dental"] = 0.2840 : 0;
                break;
        }
        health && worker['coverage'] ? worker["charge_by_health_to_worker"] = worker["charge_by_health"] - (worker["charge_by_health"] * company_percentage) : 0;
        health && worker['coverage'] ? worker["charge_by_health_to_company"] = (worker["charge_by_health"] * company_percentage) : 0;

        dental && worker['coverage'] && input['policy']['has_dental_care'] ? worker["charge_by_dental_to_worker"] = worker["charge_by_dental"] - (worker["charge_by_dental"] * company_percentage) : null;
        dental && worker['coverage'] && input['policy']['has_dental_care'] ? worker["charge_by_dental_to_company"] = (worker["charge_by_dental"] * company_percentage) : null;

        health && worker['coverage'] ? total_charge_by_health += worker["charge_by_health"] * company_percentage : null;
        dental && worker['coverage'] && input['policy']['has_dental_care'] ? total_charge_by_dental += worker["charge_by_dental"] * company_percentage : null;

        input['policy']['workers'][index] = worker;
    });

    health ? input['policy']['total_charge_by_health'] = total_charge_by_health : null;
    dental && input['policy']['has_dental_care'] ? input['policy']['total_charge_by_dental'] = total_charge_by_dental : 0;
    input['policy']['total_charge_to_company'] = 0;
    input['policy']['total_charge_to_company'] += (input['policy']['total_charge_by_health'] != undefined) ? input['policy']['total_charge_by_health'] : 0;
    input['policy']['total_charge_to_company'] += (input['policy']['total_charge_by_dental'] != undefined) ? input['policy']['total_charge_by_dental'] : 0;
    return input;
}


module.exports = calc_all;