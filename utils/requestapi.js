var unirest = require('unirest');

async function requestapi() {
    return new Promise((resolve, reject) => {
        unirest('GET', 'https://dn8mlk7hdujby.cloudfront.net/interview/insurance/policy').end(res => {
            if (res.error) reject(res.error);
             resolve(res.raw_body);
        });
    });
}

module.exports = requestapi;