# Test de programación - Bice Vida Lab
---
[![pipeline status](https://gitlab.com/msalinas92/bicevida-lab/badges/master/pipeline.svg)](https://gitlab.com/msalinas92/bicevida-lab/-/commits/master)
---
| API | Redoc        | Status        | Descripción |
| ---------- |:-------------:|:-------------:|------------:|
| [API](https://qnpcgt0u03.execute-api.us-east-1.amazonaws.com/dev/v1/api/coverage) | [DOCUMENTACION](http://apibicelab.s3-website-us-east-1.amazonaws.com/)| [![Swagger](http://online.swagger.io/validator?url=https://gitlab.com/msalinas92/bicevida-lab/-/raw/master/swagger.yml)](https://gitlab.com/msalinas92/bicevida-lab/-/blob/master/swagger.yml) | API cálculos de costos  |


## Antecedentes :
El objetivo del test de programación es conectarse con un servicio REST para obtener una
póliza de seguro ficticia sobre la cual se requieren realizar ciertos cálculos y retornarlos
también como un servicios REST.


# Solución :
Crear API utilizando ServerLess Framework sobre AWS (Utilizando AWS Lambda y API Gateway), la URL utilizada para la prueba es :
***Permite obtener el detalle del calculo de la poliza para la empresa***

- ***GET*** [https://qnpcgt0u03.execute-api.us-east-1.amazonaws.com/dev/v1/api/coverage](https://qnpcgt0u03.execute-api.us-east-1.amazonaws.com/dev/v1/api/coverage)

`curl --location --request GET 'https://qnpcgt0u03.execute-api.us-east-1.amazonaws.com/dev/v1/api/coverage'`




***Permite obtener el detalle del calculo de la poliza para la empresa filtrando por tipo de cobertura***

- ***POST*** [https://qnpcgt0u03.execute-api.us-east-1.amazonaws.com/dev/v1/api/coverage](https://qnpcgt0u03.execute-api.us-east-1.amazonaws.com/dev/v1/api/coverage)
***Filtro por cobertura salud/vida***
```
$curl --location --request POST 'https://qnpcgt0u03.execute-api.us-east-1.amazonaws.com/dev/v1/api/coverage' \
--header 'Content-Type: application/json' \
--data-raw '{ "type": "healthlife" }'
```

***Filtro por cobertura dental***
```
$curl --location --request POST 'https://qnpcgt0u03.execute-api.us-east-1.amazonaws.com/dev/v1/api/coverage' \
--header 'Content-Type: application/json' \
--data-raw '{ "type": "dental" }'
```



***Permite obtener el resumen del calculo de la poliza para la empresa***

- ***GET*** [https://qnpcgt0u03.execute-api.us-east-1.amazonaws.com/dev/v1/api/coverage/summary](https://qnpcgt0u03.execute-api.us-east-1.amazonaws.com/dev/v1/api/coverage/summary)

`curl --location --request GET 'https://qnpcgt0u03.execute-api.us-east-1.amazonaws.com/dev/v1/api/coverage/summary'`





--- 
### RESPUESTA DE LA API 
La api entrega la respuesta de API original anexando los costos por trabajador en cada uno de los ***"workers"*** anexando los valores en siguientes campos :
- coverage: Indica si el trabajador tiene cobertura
- charge_by_health : Costos cobertura salud/vida por worker
- charge_by_dental : Costo cobertura total dental por worker
- charge_by_health_to_worker: Costo cobertura total salud/vida por worker a costas del trabajador
- charge_by_health_to_company: Costo cobertura total salud/vida por worker a costas del la empresa
- charge_by_dental_to_worker: Costo cobertura total dental por worker a costas del trabajador
- charge_by_dental_to_company: Costo cobertura total dental por worker a costas del la empresa
- total_charge_by_health: Costo por salud del total de los empleados para la empresa
- total_charge_by_dental: Costo por cobertura dental del total de los empleados para la empresa
- total_charge_to_company: Costo total para la empresa




##### EJEMPLO:
```  
"policy": {
    "workers": [
        {
            "age": 35,
            "childs": 0,
            "coverage": true,
            "charge_by_health": 0.279,
            "charge_by_dental": 0.12,
            "charge_by_health_to_worker": 0.02789999999999998,
            "charge_by_health_to_company": 0.25110000000000005,
            "charge_by_dental_to_worker": 0.011999999999999997,
            "charge_by_dental_to_company": 0.108
        }
        ...
    ],
    "has_dental_care": false,
    "company_percentage": 80,
    "total_charge_by_health": 8.91904,
    "total_charge_to_company": 8.91904
}
``` 



### DOCUMENTACION DE API
Puedes ver la documentación de la API (Construida con REDOC):
[http://apibicelab.s3-website-us-east-1.amazonaws.com/](http://apibicelab.s3-website-us-east-1.amazonaws.com/)



### EJECUCION EN AMBIENTE LOCAL 

Serverless framework (con el cual esta desarrollada la API de calculo) permite ejecutar las funciones en local, para esto debes ejecutar el comando :

***1.- Clonar repositorio***
`git clone https://gitlab.com/msalinas92/bicevida-lab.git`

***2.- Instala serverless framework (puedes utilizar NPM si ya esta instalado en tu equipo)***
`$npm install -g serverless`

***3.- Instalar las dependencias del package.json***
`$npm install`

***4.- Ejecutar `serverless offline` con npm start***
`$npm start`




***Nota: 'npm start' ejecuta serverless offline***

Esto permite correr la API en la siguiente URL [http://localhost:3000/dev/v1/api/coverage]([https://link](http://localhost:3000/dev/v1/api/coverage))




### CI/CD de la API
La API construida en ServerLess pasa por un flujo de paso a producción utilizando GitLab CI/CD el cual consiste en 2 pasos la generación de la documentación y el despliege a AWS con ServerLess Framework (el archivo del pipeline es : pileline)




***pileline :***
``` 
stages:
  - doc
  - deploy

deploy:
  image: "node:12.16.1"
  stage: deploy
  script:
  - npm install -g serverless
  - npm install
  - serverless config credentials --provider aws --key ${AWS_ACCESS_KEY} --secret ${AWS_SECRET_KEY} --region ${AWS_REGION} --overwrite
  - serverless deploy
  only:
  - master

docgen:
  image: python:3.7.7-buster
  stage: doc
  script:
  - curl -sL https://deb.nodesource.com/setup_12.x | bash -
  - apt-get install -y nodejs
  - npm install -g redoc-cli
  - echo  "GENERAR DOCUMENTACION EN http://apibicelab.s3-website-us-east-1.amazonaws.com/"
  - redoc-cli bundle -o bicevidalab.html swagger.yml
  - pip3 install awscli 
  - aws configure set aws_access_key_id "${AWS_ACCESS_KEY}"
  - aws configure set aws_secret_access_key "${AWS_SECRET_KEY}"
  - aws s3 cp bicevidalab.html s3://apibicelab/ 
  - aws s3api put-object-acl --bucket apibicelab --key bicevidalab.html --acl public-read
  only:
  - master

``` 
