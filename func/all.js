'use strict';

var requestapi = require('../utils/requestapi');
var calc = require('../utils/calc');

module.exports.func = async event => {
    let resquest_str = await requestapi();
    let total_policy = await calc(JSON.parse(resquest_str),true,true);
    let response_body = JSON.stringify(total_policy);
    return {
        statusCode: 200,
        body: response_body
    };
};
