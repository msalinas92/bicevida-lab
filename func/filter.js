'use strict';

var requestapi = require('../utils/requestapi');
var calc = require('../utils/calc');


module.exports.func = async event => {
  if (!event.body) return { statusCode: 500, body: 'Parametro faltante' };
  let body = JSON.parse(event.body);
  if (!body.type) return { statusCode: 500, body: 'Parametro faltante' };

  let resquest_str = await requestapi();
  console.log(body.type);
  let total_policy = await calc(JSON.parse(resquest_str), (body.type == 'healthlife'), (body.type == 'dental'));
  let response_body = JSON.stringify(total_policy);
  return {
      statusCode: 200,
      body: response_body
  };
};
