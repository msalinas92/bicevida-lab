'use strict';

var requestapi = require('../utils/requestapi');
var calc = require('../utils/calc');


module.exports.func = async event => {
  let resquest_str = await requestapi();
  let total_policy = await calc(JSON.parse(resquest_str),true, true);

  let response_body = JSON.stringify({
    'has_dental_care' : total_policy.policy.has_dental_care,
    'company_percentage' : total_policy.policy.company_percentage,
    'total_charge_by_health' : total_policy.policy.total_charge_by_health,
    'total_charge_by_dental' : total_policy.policy.total_charge_by_dental,
    'total_charge_to_company': total_policy.policy.total_charge_to_company
  });
  return {
      statusCode: 200,
      body: response_body
  };
};
